import React, {useState} from "react";
import { NavLink, Outlet } from "react-router-dom";

export default function Message() {
    const [messages, setMessages] = useState([
        {id: '001', title: '消息1', content: '锄禾日当午'},
        {id: '002', title: '消息2', content: '锄禾日当2'},
        {id: '003', title: '消息3', content: '锄禾日当3'},
        {id: '004', title: '消息4', content: '锄禾日当4'}
    ])
  return (
    <div>
      <ul>
        {
            messages.map((item) => {
                return (
                    // 路由链接
                    <li key={item.id}>
                    <NavLink to={`detail/${item.id}/${item.title}/${item.content}`}>{item.title}</NavLink>&nbsp;&nbsp;
                    </li>
                )
            })
        }
      </ul>
      <hr/>
      {/* 指定路由组件的展示位置 */}
      <Outlet />
    </div>
  );
}
