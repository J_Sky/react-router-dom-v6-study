import React from 'react'
import { useParams, useMatch } from 'react-router-dom'

export default function Detail() {
    const {id, title, content} = useParams()
    // match必须写完整路径
    // const x = useMatch('/home/message/detail/:id/:title/:content')
    // console.log(x);
  return (
    <ul>
        <li>消息的编号：{id}</li>
        <li>消息的标题：{title}</li>
        <li>消息的的内容:{content}</li>
    </ul>
  )
}
