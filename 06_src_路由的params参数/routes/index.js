import About from "../pages/About"
import Home from "../pages/Home"
import { Navigate } from 'react-router-dom'

import New from "../pages/New";
import Message from "../pages/Message";
import Detail from "../pages/Detail";

// 路由表
const routes = [
    {
        path: '/about',
        element: <About/>
    },
    {
        path: '/home',
        element: <Home/>,
        children: [
            {
                path: 'news',
                element: <New />
            },
            {
                path: 'message',
                element: <Message />,
                children: [
                    {
                        path: 'detail/:id/:title/:content',
                        element: <Detail/>
                    }
                ]
            }
        ]
    },
    {
        path: '/',
        element: <Navigate to="/about" />
    }
]

export default routes